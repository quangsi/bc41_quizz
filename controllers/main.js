import { question_list as data_question } from '../data/data_question.js';
import {
  checkFillInput,
  checkSingleChoice,
  renderQuestion,
} from './controller.js';
let currentIndex = 0;

let question_list = data_question.map((item) => {
  // tạo ra object mới từ object cũ, và bổ sung thêm key isCorrect
  return { ...item, isCorrect: null };
});

// render lần đầu
renderQuestion(currentIndex, question_list);

// khi user nhấn câu tiếp theo
let nextQuestion = () => {
  // kiểm tra lựa chọn user
  if (question_list[currentIndex].questionType == 1) {
    question_list[currentIndex].isCorrect = checkSingleChoice(
      question_list[currentIndex].answers
    );
    console.log(question_list);
  } else {
    question_list[currentIndex].isCorrect = checkFillInput();
  }
  currentIndex++;
  renderQuestion(currentIndex, question_list);
};
window.nextQuestion = nextQuestion;
