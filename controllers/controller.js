export const renderTitle = (currentIndex, total) => {
  document.getElementById('currentStep').innerText = `${
    currentIndex + 1
  }/${total}`;
};

export let checkSingleChoice = (answers) => {
  let id = document.querySelector('input[name="singleChoice"]:checked').value;
  console.log(`  🚀: checkSingleChoice -> id`, id);
  let index = answers.findIndex((item) => {
    return item.id == id;
  });

  return answers[index].exact;
};

let renderSingleChoice = (question) => {
  let contentRadio = '';
  question.answers.forEach((item) => {
    let content = ` <div class="form-check">
      <label class="form-check-label">
        <input type="radio" class="form-check-input" name="singleChoice" id="" value=${item.id} >
      ${item.content}
      </label>
    </div>`;
    contentRadio += content;
  });
  document.getElementById('contentQuiz').innerHTML = `
  <h5>${question.content}</h5>
  ${contentRadio}
  `;
};
export const checkFillInput = () => {
  let { value, dataset } = document.getElementById('fillInput');
  console.log(`  🚀: checkFillInput ->  value, dataset `, value, dataset);
  return value == dataset.noAnswer;
};
let renderFillInput = (question) => {
  let contentHTML = ` <div class="form-group">
    <input type="text"
      class="form-control" data-no-answer='${question.answers[0].content}'  name="" id="fillInput" >
  </div>`;
  document.getElementById('contentQuiz').innerHTML = `
  <h5>${question.content}</h5>
  ${contentHTML}
  `;
};
export const renderQuestion = (currentIndex, question_list) => {
  let question = question_list[currentIndex];
  // render stt câu hỏi
  renderTitle(currentIndex, question_list.length);
  //   render nội dung câu hỏi
  if (question.questionType == 1) {
    renderSingleChoice(question);
  } else {
    renderFillInput(question);
  }
};
